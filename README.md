Выполнить один раз:

* `make` для генерации конфигов
* `sed -i -e 's/SESSION_ID/<YOUR_SESSION_ID>/g' files/flexget/config.yml` Нужно вставить SESSION_ID с https://lostfilm.tv/login

Список сериалов в нужном формате можно отредактировать в `files/flexget/config.yml` или через админку

Админки:

* http://localhost:9091/transmission/web/ - управление загрузками
* http://localhost:5050/ - управление подписками
