ifneq (,$(wildcard .env))
	include .env
endif


DOWNLOAD_DIR=$(shell xdg-user-dir DOWNLOAD)
cwd = $(shell pwd)
EXAMPLE_FILES = $(shell find $(cwd) -type f -name '*.example')

.PHONY: all
all:
	@echo "Копирую все найденые примеры файлов конфигураций"
	@echo "!!ОСТОРОЖНО!! Все изменения будут перезаписаны"
	@$(foreach i,$(EXAMPLE_FILES),cp -i $(i) $(i:%.example=%);)

##help:	Показать эту помощь
.PHONY: help
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'


